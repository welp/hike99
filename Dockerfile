FROM debian:10.12-slim

RUN addgroup --system --gid 10001 hiker
RUN adduser --system --ingroup hiker --uid 10000 --home /opt/hike hiker

USER hiker
WORKDIR /opt/hike

COPY --chown=10000:10001 server /opt/hike/

EXPOSE 7777
CMD /opt/hike/ASH_Linux.x86_64 -nographics -batchmode

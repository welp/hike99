# A Short Chart

this is a helm chart for deploying the [multiplayer server for A Short
Hike](https://adamgryu.itch.io/a-short-hike-99-online-mod).

1. create a custom values file or modify `chart/values.yaml`
1. run e.g. `helm install [-n namespace] hike ./chart [-f extra-values.yaml]`
1. open port 7777 on the cluster. if you’re using the community
   [`ingress-nginx`](https://github.com/kubernetes/ingress-nginx/), that means
   adding a `tcp` block to your ingress deployment:
   ```
   tcp:
      7777: "namespace/hike:7777"
   ```

by default it uses [the docker image i
published](https://hub.docker.com/repository/docker/grumble/hike99), but you can
build your own by downloading the server from itch and extracting it to
`./server`.

(if you want to take the joke even farther you could try to figure out how to
use alpine as the base image instead of debian to save about 70mb)
